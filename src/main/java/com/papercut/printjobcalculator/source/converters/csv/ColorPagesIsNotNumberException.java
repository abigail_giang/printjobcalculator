package com.papercut.printjobcalculator.source.converters.csv;

import com.papercut.printjobcalculator.source.converters.PrintJobsDataConvertingException;

public class ColorPagesIsNotNumberException extends PrintJobsDataConvertingException {
    public ColorPagesIsNotNumberException(Throwable e) {
        super(e);
    }
}
