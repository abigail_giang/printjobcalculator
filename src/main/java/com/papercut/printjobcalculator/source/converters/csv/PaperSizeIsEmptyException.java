package com.papercut.printjobcalculator.source.converters.csv;

import com.papercut.printjobcalculator.source.converters.PrintJobsDataConvertingException;

public class PaperSizeIsEmptyException extends PrintJobsDataConvertingException {}
