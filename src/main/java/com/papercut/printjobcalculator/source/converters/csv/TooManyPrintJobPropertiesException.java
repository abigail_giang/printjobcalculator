package com.papercut.printjobcalculator.source.converters.csv;

import com.papercut.printjobcalculator.source.converters.PrintJobsDataConvertingException;

public class TooManyPrintJobPropertiesException extends PrintJobsDataConvertingException {}