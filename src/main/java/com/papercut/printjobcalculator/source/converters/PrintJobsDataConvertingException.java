package com.papercut.printjobcalculator.source.converters;

public class PrintJobsDataConvertingException extends RuntimeException {

    public PrintJobsDataConvertingException() {}

    public PrintJobsDataConvertingException(Throwable e) {
        super(e);
    }
}