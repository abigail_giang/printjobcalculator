package com.papercut.printjobcalculator.source.converters.csv;

import com.papercut.printjobcalculator.domain.printjob.InvalidPrintJobException;
import com.papercut.printjobcalculator.domain.printjob.PrintJob;
import com.papercut.printjobcalculator.source.converters.PrintJobsDataConverter;
import com.papercut.printjobcalculator.source.converters.PrintJobsDataConvertingException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class CsvDataConverter implements PrintJobsDataConverter {

    @Override
    public List<PrintJob> invoke(String data) throws PrintJobsDataConvertingException {
        try {
            Scanner scanner = new Scanner(data);
            List<PrintJob> printJobs = new ArrayList<>();
            while (scanner.hasNextLine()) {
                String[] row = split(scanner.nextLine());
                String paperSize = extractPaperSize(row[0]);
                int colorPages = extractColorPages(row[1]);
                int totalPages = extractTotalPages(row[2]);
                boolean doubleSided = extractDoubleSided(row[3]);
                PrintJob printJob = new PrintJob(paperSize, colorPages, totalPages, doubleSided);
                printJobs.add(printJob);
            }
            return printJobs;
        } catch (InvalidPrintJobException e) {
            throw new PrintJobsDataConvertingException(e);
        }
    }

    private String[] split(String line) {
        String[] row = line.split(",");
        if (row.length == 3 && line.endsWith(",")) {
            row  = Stream.concat(Stream.of(row), Stream.of("")).toArray(String[]::new);
        }
        if (row.length < 4) {
            throw new MissingPrintJobPropertiesException();
        }
        if (row.length > 4) {
            throw new TooManyPrintJobPropertiesException();
        }
        return row;
    }

    private int extractTotalPages(String value) {
        if (value.isEmpty()) {
            throw new TotalPagesIsEmptyException();
        }
        if (value.trim().isEmpty()) {
            throw new TotalPagesIsBlankException();
        }
        try {
            int pages = Integer.parseInt(value.trim());
            if (pages < 0) {
                throw new TotalPagesIsNegativeException();
            }
            return pages;
        } catch (NumberFormatException e) {
            throw new TotalPagesIsNotNumberException();
        }
    }

    private boolean extractDoubleSided(String value) {
        if (value.isEmpty()) {
            throw new DoubleSidedIsEmptyException();
        }
        String doubleSided = value.trim();
        if (doubleSided.isEmpty()) {
            throw new DoubleSidedIsBlankException();
        }
        if (doubleSided.equalsIgnoreCase("y") || doubleSided.equalsIgnoreCase("true")) {
            return Boolean.TRUE;
        } else if (doubleSided.equalsIgnoreCase("n") || doubleSided.equalsIgnoreCase("false")) {
            return Boolean.FALSE;
        } else {
            throw new DoubleSidedIsNotBooleanException();
        }
    }

    private int extractColorPages(String value) {
        if (value.isEmpty()) {
            throw new ColorPagesIsEmptyException();
        }
        if (value.trim().isEmpty()) {
            throw new ColorPagesIsBlankException();
        }
        try {
            int colorPages = Integer.parseInt(value.trim());
            if (colorPages < 0) {
                throw new ColorPagesIsNegativeException();
            }
            return colorPages;
        } catch (NumberFormatException e) {
            throw new ColorPagesIsNotNumberException(e);
        }
    }

    private String extractPaperSize(String value) {
        if (value.isEmpty()) {
            throw new PaperSizeIsEmptyException();
        }
        if (value.trim().isEmpty()) {
            throw new PaperSizeIsBlankException();
        }
        return value.trim();
    }
}