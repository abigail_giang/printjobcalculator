package com.papercut.printjobcalculator.source.converters;

import com.papercut.printjobcalculator.domain.printjob.PrintJob;

import java.util.List;

@FunctionalInterface
public interface PrintJobsDataConverter {

    List<PrintJob> invoke(String data) throws PrintJobsDataConvertingException;
}
