package com.papercut.printjobcalculator.source.readers;

public class PrintJobDataReadingException extends RuntimeException {

    public PrintJobDataReadingException() {}

    public PrintJobDataReadingException(Throwable e) {
        super(e);
    }
}
