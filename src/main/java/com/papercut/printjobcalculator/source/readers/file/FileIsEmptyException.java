package com.papercut.printjobcalculator.source.readers.file;

import com.papercut.printjobcalculator.source.readers.PrintJobDataReadingException;

public class FileIsEmptyException extends PrintJobDataReadingException {}
