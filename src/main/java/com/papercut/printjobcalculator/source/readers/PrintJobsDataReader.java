package com.papercut.printjobcalculator.source.readers;

import com.papercut.printjobcalculator.domain.printjob.PrintJob;

import java.util.List;

@FunctionalInterface
public interface PrintJobsDataReader {

    List<PrintJob> invoke() throws PrintJobDataReadingException;
}
