package com.papercut.printjobcalculator.source.readers.file;

import com.papercut.printjobcalculator.domain.printjob.PrintJob;
import com.papercut.printjobcalculator.source.converters.PrintJobsDataConverter;
import com.papercut.printjobcalculator.source.readers.PrintJobDataReadingException;
import com.papercut.printjobcalculator.source.readers.PrintJobsDataReader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


public class FileDataReader implements PrintJobsDataReader {

    private final File source;
    private final PrintJobsDataConverter converter;

    public FileDataReader(File source, PrintJobsDataConverter converter) {
        if (source == null) {
            throw new NullPointerException("Source cannot be null.");
        }
        if (converter == null) {
            throw new NullPointerException("Converter cannot be null.");
        }
        this.source = source;
        this.converter = converter;
    }

    @Override
    public List<PrintJob> invoke() {
        if (!source.exists()) {
            throw new FileDoesNotExistException();
        }
        if (source.length() == 0) {
            throw new FileIsEmptyException();
        }
        String fileContent;
        try {
            fileContent = new String(Files.readAllBytes(Paths.get(source.getAbsolutePath())));
        } catch (IOException e) {
            throw new PrintJobDataReadingException(e);
        }
        if (fileContent.trim().isEmpty()) {
            throw new FileIsEmptyException();
        }
        return converter.invoke(fileContent);
    }
}
