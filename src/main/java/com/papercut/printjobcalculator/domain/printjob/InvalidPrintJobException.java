package com.papercut.printjobcalculator.domain.printjob;

public class InvalidPrintJobException extends Exception {

    public InvalidPrintJobException(String message) {
        super(message);
    }
}
