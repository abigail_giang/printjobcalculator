package com.papercut.printjobcalculator.domain.printjob;

import java.util.Objects;

public final class PrintJob {

    private final String paperSize;
    private final int colorPages;
    private final int totalPages;
    private final boolean doubleSided;

    public PrintJob(String paperSize, int colorPages, int totalPages, boolean doubleSided) throws InvalidPrintJobException {
        this.totalPages = totalPages;
        this.colorPages = colorPages;
        this.doubleSided = doubleSided;
        this.paperSize = paperSize;
        validate();
    }

    private void validate() throws InvalidPrintJobException {
        if (totalPages < colorPages) {
            throw new InvalidPrintJobException("Total pages must be greater than or equal to color.");
        }
    }

    public String getPaperSize() {
        return paperSize;
    }

    public int getColorPages() {
        return colorPages;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public boolean isDoubleSided() {
        return doubleSided;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrintJob printJob = (PrintJob) o;
        return getColorPages() == printJob.getColorPages() &&
                getTotalPages() == printJob.getTotalPages() &&
                isDoubleSided() == printJob.isDoubleSided() &&
                Objects.equals(getPaperSize(), printJob.getPaperSize());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getPaperSize(), getColorPages(), getTotalPages(), isDoubleSided());
    }
}
