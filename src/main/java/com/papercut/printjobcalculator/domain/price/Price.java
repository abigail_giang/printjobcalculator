package com.papercut.printjobcalculator.domain.price;

import java.math.BigDecimal;
import java.util.Objects;

public class Price {

    private final String paperSize;
    private final BigDecimal singleGrayscale;
    private final BigDecimal doubleGrayscale;
    private final BigDecimal singleColor;
    private final BigDecimal doubleColor;

    public Price(
            String paperSize,
            BigDecimal singleGrayscale,
            BigDecimal doubleGrayscale,
            BigDecimal singleColor,
            BigDecimal doubleColor) {
        if (paperSize == null) {
            throw new NullPointerException("Papersize cannot be null.");
        }
        if (singleGrayscale == null) {
            throw new NullPointerException("Single grayscale price cannot be null.");
        }
        if (doubleGrayscale == null) {
            throw new NullPointerException("Double grayscale price cannot be null.");
        }
        if (singleColor == null) {
            throw new NullPointerException("Single color price cannot be null.");
        }
        if (doubleColor == null) {
            throw new NullPointerException("Double color price cannot be null.");
        }
        this.paperSize = paperSize;
        this.singleGrayscale = singleGrayscale;
        this.doubleGrayscale = doubleGrayscale;
        this.singleColor = singleColor;
        this.doubleColor = doubleColor;
    }

    public String getPaperSize() {
        return paperSize;
    }

    public BigDecimal getSingleGrayscale() {
        return singleGrayscale;
    }

    public BigDecimal getDoubleGrayscale() {
        return doubleGrayscale;
    }

    public BigDecimal getSingleColor() {
        return singleColor;
    }

    public BigDecimal getDoubleColor() {
        return doubleColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Objects.equals(getPaperSize(), price.getPaperSize()) &&
                Objects.equals(getSingleGrayscale(), price.getSingleGrayscale()) &&
                Objects.equals(getDoubleGrayscale(), price.getDoubleGrayscale()) &&
                Objects.equals(getSingleColor(), price.getSingleColor()) &&
                Objects.equals(getDoubleColor(), price.getDoubleColor());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getPaperSize(), getSingleGrayscale(), getDoubleGrayscale(), getSingleColor(), getDoubleColor());
    }
}
