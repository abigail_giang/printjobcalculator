package com.papercut.printjobcalculator.domain.price;


public interface PriceRepository {
    Price find(String paperSize) throws PriceRepositoryExceptions;
}
