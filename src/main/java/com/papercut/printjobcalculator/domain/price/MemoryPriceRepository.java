package com.papercut.printjobcalculator.domain.price;

import java.math.BigDecimal;

public class MemoryPriceRepository implements PriceRepository {

    @Override
    public Price find(String paperSize) {
        if (paperSize == null) {
            throw new NullPointerException("Papersize cannot be null.");
        }
        return new Price("A4",
                new BigDecimal(1.0), new BigDecimal(0.8),
                new BigDecimal(0.5), new BigDecimal(0.4)
        );
    }
}
