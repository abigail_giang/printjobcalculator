package com.papercut.printjobcalculator.source;

import com.papercut.printjobcalculator.domain.printjob.PrintJob;
import com.papercut.printjobcalculator.source.converters.PrintJobsDataConverter;
import com.papercut.printjobcalculator.source.converters.csv.CsvDataConverter;
import com.papercut.printjobcalculator.source.readers.file.FileDataReader;
import com.papercut.printjobcalculator.source.readers.file.FileDoesNotExistException;
import com.papercut.printjobcalculator.source.readers.file.FileIsEmptyException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.util.Lists.list;

public class FileDataReaderTest {

    @Rule public final TemporaryFolder temp = new TemporaryFolder();

    private final PrintJobsDataConverter converter = new CsvDataConverter();

    @Test
    @SuppressWarnings("ConstantConditions")
    public void should_throw_exception_when_source_is_null() {
        assertThat(
                catchThrowable(() -> new FileDataReader(null, converter))
        ).isInstanceOf(NullPointerException.class);
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    public void should_throw_exception_when_converter_is_null() throws Exception {
        File source = temp.newFile();
        Files.write(source.toPath(), list(
                "A4,1,3,true"
        ));
        assertThat(
                catchThrowable(() -> new FileDataReader(source, null))
        ).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void should_throw_exception_when_file_does_not_exist() throws Exception {
        File source = temp.newFile();
        assertThat(source.delete()).isTrue();
        assertThat(
                catchThrowable(() -> new FileDataReader(source, converter).invoke())
        ).isInstanceOf(FileDoesNotExistException.class);
    }

    @Test
    public void should_throw_exception_when_file_is_empty() throws Exception {
        File source = temp.newFile();
        Files.write(source.toPath(), list(
                "",
                ""
        ));
        assertThat(
                catchThrowable(() -> new FileDataReader(source, converter).invoke())
        ).isInstanceOf(FileIsEmptyException.class);
    }

    @Test
    public void should_return_print_jobs() throws Exception {
        File source = temp.newFile();
        Files.write(source.toPath(), list(
                "A4,0,10,Y",
                "A3,1,9,N",
                "LETTER,2,8,Y",
                "LEGAL,3,7,N",
                "A4,4,6,N"
        ));
        List<PrintJob> printJobs = new FileDataReader(source, converter).invoke();
        assertThat(printJobs).containsExactly(
                new PrintJob("A4", 0, 10, true),
                new PrintJob("A3", 1, 9, false),
                new PrintJob("LETTER", 2, 8, true),
                new PrintJob("LEGAL", 3, 7, false),
                new PrintJob("A4", 4, 6, false)
        );
    }
}