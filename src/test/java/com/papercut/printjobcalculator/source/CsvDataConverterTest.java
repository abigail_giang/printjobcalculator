package com.papercut.printjobcalculator.source;

import com.papercut.printjobcalculator.domain.printjob.InvalidPrintJobException;
import com.papercut.printjobcalculator.domain.printjob.PrintJob;
import com.papercut.printjobcalculator.source.converters.csv.ColorPagesIsBlankException;
import com.papercut.printjobcalculator.source.converters.csv.ColorPagesIsEmptyException;
import com.papercut.printjobcalculator.source.converters.csv.ColorPagesIsNegativeException;
import com.papercut.printjobcalculator.source.converters.csv.ColorPagesIsNotNumberException;
import com.papercut.printjobcalculator.source.converters.csv.CsvDataConverter;
import com.papercut.printjobcalculator.source.converters.csv.DoubleSidedIsBlankException;
import com.papercut.printjobcalculator.source.converters.csv.DoubleSidedIsEmptyException;
import com.papercut.printjobcalculator.source.converters.csv.DoubleSidedIsNotBooleanException;
import com.papercut.printjobcalculator.source.converters.csv.MissingPrintJobPropertiesException;
import com.papercut.printjobcalculator.source.converters.csv.PaperSizeIsBlankException;
import com.papercut.printjobcalculator.source.converters.csv.PaperSizeIsEmptyException;
import com.papercut.printjobcalculator.source.converters.csv.TooManyPrintJobPropertiesException;
import com.papercut.printjobcalculator.source.converters.csv.TotalPagesIsBlankException;
import com.papercut.printjobcalculator.source.converters.csv.TotalPagesIsEmptyException;
import com.papercut.printjobcalculator.source.converters.csv.TotalPagesIsNegativeException;
import com.papercut.printjobcalculator.source.converters.csv.TotalPagesIsNotNumberException;
import org.assertj.core.util.Lists;
import org.junit.Test;

import java.util.List;

import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.Assertions.tuple;

public class CsvDataConverterTest {

    private final CsvDataConverter converter = new CsvDataConverter();

    @Test
    public void should_throw_exception_when_an_data_is_null() {
        assertThat(
                catchThrowable(() -> converter.invoke(null))
        ).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void should_throw_exception_when_row_has_less_than_4_columns() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4, 1, 2"))
        ).isInstanceOf(MissingPrintJobPropertiesException.class);
    }

    @Test
    public void should_throw_exception_when_row_has_more_than_4_columns() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4, 1, 3, true, extra_field"))
        ).isInstanceOf(TooManyPrintJobPropertiesException.class);
    }

    @Test
    public void should_throw_exception_when_paper_size_is_empty() {
        assertThat(
                catchThrowable(() -> converter.invoke(",3,10,false"))
        ).isInstanceOf(PaperSizeIsEmptyException.class);
    }

    @Test
    public void should_throw_exception_when_paper_size_is_blank() {
        assertThat(
                catchThrowable(() -> converter.invoke(" ,100,200,true"))
        ).isInstanceOf(PaperSizeIsBlankException.class);
    }

    @Test
    public void should_throw_exception_when_color_pages_is_empty() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4,,100,true"))
        ).isInstanceOf(ColorPagesIsEmptyException.class);
    }

    @Test
    public void should_throw_exception_when_color_pages_is_blank() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4, ,100,true"))
        ).isInstanceOf(ColorPagesIsBlankException.class);
    }

    @Test
    public void should_throw_exception_when_color_pages_is_not_a_number() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4,aaa,100,true"))
        ).isInstanceOf(ColorPagesIsNotNumberException.class);
    }

    @Test
    public void should_throw_exception_when_color_pages_is_negative() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4,-10,100,true"))
        ).isInstanceOf(ColorPagesIsNegativeException.class);
    }

    @Test
    public void should_throw_exception_when_total_pages_is_empty() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4, 10,, true"))
        ).isInstanceOf(TotalPagesIsEmptyException.class);
    }

    @Test
    public void should_throw_exception_when_total_pages_is_blank() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4,10, ,true"))
        ).isInstanceOf(TotalPagesIsBlankException.class);
    }

    @Test
    public void should_throw_exception_when_total_pages_is_not_a_number() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4,10,aaa,true"))
        ).isInstanceOf(TotalPagesIsNotNumberException.class);
    }

    @Test
    public void should_throw_exception_when_total_pages_is_negative() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4,10,-100,true"))
        ).isInstanceOf(TotalPagesIsNegativeException.class);

    }

    @Test
    public void should_throw_exception_when_double_sided_is_empty() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4,10,100,"))
        ).isInstanceOf(DoubleSidedIsEmptyException.class);
    }

    @Test
    public void should_throw_exception_when_double_sided_is_blank() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4,10,100, "))
        ).isInstanceOf(DoubleSidedIsBlankException.class);
    }

    @Test
    public void should_throw_exception_when_double_sided_is_not_a_boolean() {
        assertThat(
                catchThrowable(() -> converter.invoke("A4,10,100,Incorrect"))
        ).isInstanceOf(DoubleSidedIsNotBooleanException.class);
    }

    @Test
    public void should_trim_field_values_and_return_print_job() throws InvalidPrintJobException {
        List<PrintJob> printJobs = converter.invoke(" A3 , 10 , 20 , false ");
        assertThat(printJobs).containsExactly(
                new PrintJob ("A3",10,20,false)
        );
    }

    @Test
    public void should_return_print_jobs() {
        String csv = Lists
                .list(
                        "A4,10,100,true",
                        "A3,1,2,N",
                        "A3,33,44,Y",
                        "A4,10,10,false"
                )
                .stream()
                .collect(joining("\n"));
        List<PrintJob> printJobs = converter.invoke(csv);
        assertThat(printJobs)
                .extracting(
                        PrintJob::getPaperSize,
                        PrintJob::getColorPages,
                        PrintJob::getTotalPages,
                        PrintJob::isDoubleSided
                )
                .containsExactlyInAnyOrder(
                        tuple("A4", 10, 100, true),
                        tuple("A3", 1, 2, false),
                        tuple("A3", 33, 44, true),
                        tuple("A4", 10, 10, false)
                );
    }
}
