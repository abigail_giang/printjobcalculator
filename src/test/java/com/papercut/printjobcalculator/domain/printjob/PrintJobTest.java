package com.papercut.printjobcalculator.domain.printjob;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class PrintJobTest {

    @Test
    public void should_create_print_job() throws Exception {
        PrintJob actual = new PrintJob("any", 10, 15, true);
        assertThat(actual.getPaperSize()).isEqualTo("any");
        assertThat(actual.getColorPages()).isEqualTo(10);
        assertThat(actual.getTotalPages()).isEqualTo(15);
        assertThat(actual.isDoubleSided()).isTrue();
    }

    @Test
    public void should_throw_exception_when_color_pages_is_greater_than_total() {
        assertThat(
                catchThrowable(() -> new PrintJob("any", 15, 10, true))
        ).isInstanceOf(InvalidPrintJobException.class);
    }
}
