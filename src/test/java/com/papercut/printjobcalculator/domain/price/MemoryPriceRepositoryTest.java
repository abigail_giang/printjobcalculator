package com.papercut.printjobcalculator.domain.price;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Comparator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.Assertions.tuple;

public class MemoryPriceRepositoryTest {

    private final PriceRepository repository = new MemoryPriceRepository();

    @Test
    public void should_return_price_list_of_a_paper_size() {
        assertThat(repository.find("A4"))
                .isEqualTo(new Price("A4",
                        new BigDecimal(1.0), new BigDecimal(0.8),
                        new BigDecimal(0.5), new BigDecimal(0.4)));

    }

    @Test
    public void should_throw_exception_for_null_paper_size() {
        assertThat(
                catchThrowable(() -> repository.find(null))
        ).isInstanceOf(NullPointerException.class);
    }

}
