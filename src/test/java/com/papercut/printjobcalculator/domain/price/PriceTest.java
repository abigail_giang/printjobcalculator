package com.papercut.printjobcalculator.domain.price;

import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class PriceTest {
    @Test
    public void should_throw_exception_when_paper_size_null() {
        assertThat(
                catchThrowable(() -> new Price(null,
                        new BigDecimal(1.0), new BigDecimal(1.0),
                        new BigDecimal(1.0), new BigDecimal(1.0)))
        ).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void should_throw_exception_when_single_grayscale_price_null() {
        assertThat(
                catchThrowable(() -> new Price("A4",
                        null, new BigDecimal(1.0),
                        new BigDecimal(1.0), new BigDecimal(1.0)))
        ).isInstanceOf(NullPointerException.class);
    }
    @Test
    public void should_throw_exception_when_double_grayscale_price_null() {
        assertThat(
                catchThrowable(() -> new Price("A4",
                        new BigDecimal(1.0), null,
                        new BigDecimal(1.0), new BigDecimal(1.0)))
        ).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void should_throw_exception_when_single_color_price_null() {
        assertThat(
                catchThrowable(() -> new Price("A4",
                        new BigDecimal(1.0), new BigDecimal(1.0),
                        null, new BigDecimal(1.0)))
        ).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void should_throw_exception_when_double_color_price_null() {
        assertThat(
                catchThrowable(() -> new Price("A4",
                        new BigDecimal(1.0), new BigDecimal(1.0),
                        new BigDecimal(1.0), null))
        ).isInstanceOf(NullPointerException.class);
    }
}
